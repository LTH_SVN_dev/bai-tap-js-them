var checkNamNhuan = function (y) {
    return ((y % 4 == 0 && y % 100 != 0) || y % 400 == 0);
}
/* check số ngày trong tháng */

var outPutDay = function (){
    var m = document.getElementById("thang").value * 1
    var y = document.getElementById("nam").value * 1
    var checkDay = function (y) {
        
        
        switch (m) {
            case 1: case 3: case 5: case 7: case 8: case 10: case 12:
                {
                    return  31
                }
            case 2:
                {
                    if (checkNamNhuan(y)) {
                        return 29;
                    }
                    return 28;
                }
            case 4: case 6: case 9: case 11:
                {
                    return 30;
                }
        }
    }
    document.getElementById("result").innerHTML = ` <p>Tháng này có ${checkDay(y)} ngày</p>`
}