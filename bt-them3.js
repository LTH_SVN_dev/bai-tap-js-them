var read = function () {
    var n = document.getElementById("input").value * 1
    var tram = Math.floor(n / 100);
    var chuc = Math.floor((n % 100) / 10);
    var donVi = n % 10;
    var i, j, k
    switch (tram) {
        case 1: i = "Một trăm"; break;
        case 2: i = "Hai trăm"; break;
        case 3: i = "Ba trăm"; break;
        case 4: i = "Bốn trăm"; break;
        case 5: i = "Năm trăm"; break;
        case 6: i = "Sáu trăm"; break;
        case 7: i = "Bảy trăm"; break;
        case 8: i = "Tám trăm"; break;
        case 9: i = "Chín trăm"; break;
    }
    if (chuc % 10 == 0 && tram != 0) {
        printf("lẻ ");
    }
    switch (chuc) {
        case 1: j = "mười"; break;
        case 2: j = "hai mươi"; break;
        case 3: j = "ba mươi"; break;
        case 4: j = "bốn mươi"; break;
        case 5: j = "năm mươi"; break;
        case 6: j = "sáu mươi"; break;
        case 7: j = "bảy mươi"; break;
        case 8: j = "tám mươi"; break;
        case 9: j = "chín mươi"; break;
    }
    switch (donVi) {
        case 1: k = "một"; break;
        case 2: k = "hai"; break;
        case 3: k = "ba"; break;
        case 4: k = "bốn"; break;
        case 5: k = "năm"; break;
        case 6: k = "sáu"; break;
        case 7: k = "bảy"; break;
        case 8: k = "tám"; break;
        case 9: k = "chín"; break;
    }
    document.getElementById("result").innerHTML = `<p>Số bạn vừa nhập là ${i} ${j} ${k}</p>`
}
